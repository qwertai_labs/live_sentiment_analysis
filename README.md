# Qwert AI Labs

## Live Web crawler e análise de sentimentos.

link: https://www.youtube.com/watch?v=AgNtFZO84Ls&ab_channel=qwertai

##  IMBD reviews em português para análise de sentimento usando nltk e sklearn

### Contexto

- Este conjunto de dados tem reviews em portugues divididos em classes positivas e negativas para classificação de polaridade de sentimento.
- Este conjunto de dados é muito interessante, estamos lidando com a linguagem popular na internet.
- É necessário realizar alguns tratamentos mais cuidadosos, como remoção de ruído, redução de dimensionalidade, padronização de objetos e avaliação de alguns símbolos, se forem importantes para expressar algum sentimento.
 
### Dataset

- Reviews - 49459 
- classes - positivo e negativo

### Descrição das colunas

- id: (int) Identificador do reviews
- text_en: (string) Texto completo em inglês
- text_pt: (string) Texto completo em portuguêssentiment: (string) Rótulo de sentimento (classificador)
 
### Conteúdo

- Carregar bibliotecas.
- Carregar conjunto de dados.
- Pré-processamento de texto.
- Análise exploratória em palavras.
- Bag-of-Words.Prepare o algoritmo e execute o modelo.
- Previsões autônomas.Avalie o modelo.
- Construindo um pipeline MultinomialNB.
- Avaliando modelo MultinomialNB.

### Web Crawler

- Extraindo review de um filme na IMBD.
- Traduzir dataset para português.
- Fazer predições do dataset extraido.